package com.locationtracker.base

import android.app.Application
import android.content.ContextWrapper
import android.os.StrictMode
import com.baseutils.sharedpreference.Prefs

open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        //For Picker to avoid FIleUri Exposed Exception
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        /**
         *  SharedPreferences Initialization
         */

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()
    }
}