package com.locationtracker.models
import com.google.gson.annotations.SerializedName


data class AddTimeSheetModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)