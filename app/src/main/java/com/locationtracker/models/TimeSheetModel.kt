package com.locationtracker.models

import com.google.gson.annotations.SerializedName
import java.util.*


data class TimeSheetModel(
        @SerializedName("data")
        val data: LinkedList<Data>,
        @SerializedName("status")
        val status: Int
) {
    data class Data(
            @SerializedName("sheet_created")
            val sheetCreated: String,
            @SerializedName("sheet_date")
            val sheetDate: String,
            @SerializedName("sheet_id")
            val sheetId: String,
            @SerializedName("sheet_work")
            val sheetWork: String
    )
}

