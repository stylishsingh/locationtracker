package com.locationtracker.models

import com.google.gson.annotations.SerializedName


class RegisterModel(
        @SerializedName("email")
        val email: String,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("phone")
        val phone: String,
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("username")
        val username: String
)
