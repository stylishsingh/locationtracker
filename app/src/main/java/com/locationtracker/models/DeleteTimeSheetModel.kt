package com.locationtracker.models
import com.google.gson.annotations.SerializedName


data class DeleteTimeSheetModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)