package com.locationtracker.presentations.adapters

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.locationtracker.R
import com.locationtracker.databinding.ListItemTimeSheetBinding
import com.locationtracker.interfaces.OnItemClickListener
import com.locationtracker.models.TimeSheetModel
import kotlinx.android.synthetic.main.list_item_time_sheet.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TimeSheetAdapter(private var listItems: LinkedList<TimeSheetModel.Data>) : RecyclerView.Adapter<ViewHolder>() {

    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemTimeSheetBinding.inflate(inflater)
        return ViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return listItems.size
//        return 15
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is ViewHolder) {

            val holder = viewHolder
//
            holder.bind(listItems.get(holder.adapterPosition))
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {


        lateinit var viewListener: OnItemClickListener

        init {
            itemView.iv_delete.setOnClickListener(this)
        }

        fun bind(item: TimeSheetModel.Data) {
            setOnItemClickListener(listener!!)
            with(itemView) {
                tv_date.text = getFormattedDate(item.sheetCreated)
                tv_name.text = item.sheetWork

            }
        }

        private fun getFormattedDate(date: String): String {
            try {
                val simpleDateFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
                val simpleDateFormatServer = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
                simpleDateFormatServer.timeZone = TimeZone.getTimeZone("UTC")
                return simpleDateFormat.format(simpleDateFormatServer.parse(date))
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return ""
        }

        private fun setOnItemClickListener(listener: OnItemClickListener) {
            viewListener = listener
        }

        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.iv_delete -> {
                    viewListener.onItemClick(adapterPosition,"")
                }
            }
        }
    }

    fun updateList(listItems: LinkedList<TimeSheetModel.Data>) {
        this.listItems = listItems
        notifyDataSetChanged()
    }

}