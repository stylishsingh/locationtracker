package com.locationtracker.presentations.views.activities

import android.os.Bundle
import com.locationtracker.R
import com.locationtracker.presentations.views.fragments.RegisterFragment


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigateToWithBundle(R.id.container,RegisterFragment(),RegisterFragment::class.java.simpleName,false,null)

    }

}
