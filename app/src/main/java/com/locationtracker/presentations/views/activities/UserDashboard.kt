package com.locationtracker.presentations.views.activities

import android.content.Context
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import com.firebase.jobdispatcher.*
import com.locationtracker.R
import com.locationtracker.ScheduledJobService
import com.locationtracker.databinding.ActivityUserDashboardBinding
import com.locationtracker.presentations.views.fragments.TimeSheetFragment

class UserDashboard : BaseActivity(){

   lateinit var binding:ActivityUserDashboardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        scheduleJob(this)

        binding=DataBindingUtil.setContentView(this,R.layout.activity_user_dashboard)

        checkLocationCameraPermission()

//        navigateToWithBundle(R.id.container,TimeSheetFragment(),"",false,null)
    }

    private fun scheduleJob(context: Context) {
        //creating new fire base job dispatcher
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))

        //creating new job and adding it with dispatcher
        val job = createJob(dispatcher)

        dispatcher.mustSchedule(job)
    }

    private fun createJob(dispatcher: FirebaseJobDispatcher): Job {

        return dispatcher.newJobBuilder()
                //persist the task across boots
                .setLifetime(Lifetime.FOREVER)
                //.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                //call this service when the criteria are met.
                .setService(ScheduledJobService::class.java)
                //unique id of the task
                .setTag(getString(R.string.app_name))
                //don't overwrite an existing job with the same tag
                .setReplaceCurrent(true)
                // We are mentioning that the job is periodic.
                .setRecurring(true)
                // Run between 30 - 60 seconds from now.
                .setTrigger(Trigger.executionWindow(300, 600))
                // retry with exponential backoff
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                //.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                //Run this job only when the network is available.
                .setConstraints(Constraint.ON_ANY_NETWORK, Constraint.DEVICE_CHARGING)
                .build()
    }


    fun updateJob(dispatcher: FirebaseJobDispatcher): Job {
        return dispatcher.newJobBuilder()
                //update if any task with the given tag exists.
                .setReplaceCurrent(true)
                //Integrate the job you want to start.
                .setService(ScheduledJobService::class.java)
                .setTag(getString(R.string.app_name))
                // Run between 30 - 60 seconds from now.
                .setTrigger(Trigger.executionWindow(300, 600))
                .build()
    }

    fun cancelJob(context: Context) {

        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))
        //Cancel all the jobs for this package
        dispatcher.cancelAll()
        // Cancel the job for this tag
        dispatcher.cancel(getString(R.string.app_name))

    }

    private fun checkLocationCameraPermission() {
        if (getActivity() != null) {
            if (!PermissionsAndroid.getInstance().checkLocationPermission(getActivity())) {
                // Do not have permissions, request them now
                PermissionsAndroid.getInstance().requestForLocationPermission(this)
                return
            }
            (getActivity() as BaseActivity).showProgressDialog()
            try {
                val placeBuilder = PlacePicker.IntentBuilder()
                getActivity()!!.startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
                (getActivity() as BaseActivity).hideProgressDialog()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
                (getActivity() as BaseActivity).hideProgressDialog()
            }

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionsAndroid.LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Location Permission Granted", Toast.LENGTH_SHORT).show()
                    checkLocationCameraPermission()
                } else if (Build.VERSION.SDK_INT >= 23 && permissions.size > 0 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    println("Go to settings>>>>>>")

                } else {
                    Toast.makeText(getActivity(), "Location Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}
