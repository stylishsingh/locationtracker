package com.locationtracker.presentations.views.fragments


import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.baseutils.sharedpreference.Prefs
import com.locationtracker.R
import com.locationtracker.databinding.FragmentAddTimeSheetBinding
import com.locationtracker.models.AddTimeSheetModel
import com.locationtracker.networks.repositories.ApiProvider
import com.locationtracker.presentations.views.activities.HolderActivity
import com.locationtracker.utils.CalendarPicker
import com.locationtracker.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class AddTimeSheetFragment : Fragment(), View.OnClickListener {
    lateinit var binding: FragmentAddTimeSheetBinding

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_time_sheet, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as HolderActivity).setSupportActionBar(binding.toolbar)

        if((activity as HolderActivity).supportActionBar!=null){
            (activity as HolderActivity).supportActionBar!!.title=""
            binding.toolbar.title="Add Time Sheet"
        }

        binding.tvName.text = "Name: " + Prefs.getString(Constants.NAME, "")
        binding.tvPhone.text = "Phone: " + Prefs.getString(Constants.PHONE, "")



        binding.btnPreviousWork.setOnClickListener(this)
        binding.btnSaveWork.setOnClickListener(this)
        binding.etDate.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (activity != null) {
            when (view!!.id) {
                R.id.et_date -> {
                    CalendarPicker.instance.showCalendar(activity!!, this)
                }
                R.id.btn_save_work -> {
                    if (TextUtils.isEmpty(binding.etDate.text.toString().trim())
                            && TextUtils.isEmpty(binding.etWork.text.toString())) {
                        Toast.makeText(activity, "Please fill all the details", Toast.LENGTH_SHORT).show()
                    } else if (TextUtils.isEmpty(binding.etDate.text.toString().trim())) {
                        Toast.makeText(activity, "Please enter date", Toast.LENGTH_SHORT).show()
                    } else if (TextUtils.isEmpty(binding.etWork.text.toString().trim())) {
                        Toast.makeText(activity, "Please enter work", Toast.LENGTH_SHORT).show()
                    } else
                        saveWork()
                }
                R.id.btn_previous_work -> {
                }
            }
        }
    }

    private fun saveWork() {
        binding.includeLoader.visibility = View.VISIBLE
        val repository = ApiProvider.apiProviderRepository()
        compositeDisposable.add(
                repository.addTimeSheet(Constants.TOKEN_VALUE,
                        Prefs.getString(Constants.USER_ID, ""),
                        binding.etDate.text.toString(),
                        binding.etWork.text.toString())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<AddTimeSheetModel>() {
                            override fun onSuccess(t: AddTimeSheetModel) {
                                println("Success")
                                binding.includeLoader.visibility = View.GONE
                                activity!!.sendBroadcast(Intent(Constants.USER_SHEET))
                                activity!!.finish()
                            }

                            override fun onError(e: Throwable) {
                                binding.includeLoader.visibility = View.GONE
                                e.printStackTrace()
                            }
                        })

        )
    }

    fun setDate(selectedDate: String) {
        binding.etDate.setText(selectedDate)
    }

//    override fun onPause() {
//        super.onPause()
//        compositeDisposable.dispose()
//    }

}
