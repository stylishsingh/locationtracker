package com.locationtracker.presentations.views.activities

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast


abstract class BaseActivity : AppCompatActivity() {


    fun navigateToWithBundle(container: Int, fragment: Fragment, extraTag: String, isBackStack: Boolean, bundle: Bundle?) {
        val finalTag = fragment.javaClass.simpleName
        fragment.arguments = bundle
        val fts = supportFragmentManager.beginTransaction()
        fts.replace(container, fragment, finalTag)
        if (isBackStack)
            fts.addToBackStack(finalTag)
        fts.commit()
    }


    fun clearBackStack() {
        val fragmentManager = supportFragmentManager
        //this will clear the back stack and displays no animation on the screen
        val backStackCount = fragmentManager.backStackEntryCount
        for (i in 0 until backStackCount) {
            // Get the back stack fragment id.
            val backStackId = fragmentManager.getBackStackEntryAt(i).id
            fragmentManager.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            //super.onBackPressed();
            finish()
        }
    }

    fun isInternetAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE)) {
                if (cm != null) {
                    val activeNetwork = cm.activeNetworkInfo
                    return activeNetwork != null && activeNetwork.isConnected
                }
            } else
                return false
        } else {
            if (cm != null) {
                val activeNetwork = cm.activeNetworkInfo
                return activeNetwork != null && activeNetwork.isConnected
            }
        }
        return false
    }

}