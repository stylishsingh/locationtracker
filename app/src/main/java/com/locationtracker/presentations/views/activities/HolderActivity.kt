package com.locationtracker.presentations.views.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.locationtracker.R
import com.locationtracker.databinding.ActivityHolderBinding
import com.locationtracker.presentations.views.fragments.AddTimeSheetFragment
import com.locationtracker.utils.Constants

class HolderActivity : BaseActivity() {

    lateinit var binding: ActivityHolderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_holder)

        if (intent != null && intent.extras != null) {
            when (intent.getStringExtra(Constants.SCREEN)) {
                Constants.SCREEN_ADD_TIME_SHEET -> {
                    navigateToWithBundle(R.id.container, AddTimeSheetFragment(), "", false, null)
                }
            }
        }
    }
}
