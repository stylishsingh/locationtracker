package com.locationtracker.presentations.views.fragments

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.baseutils.sharedpreference.Prefs
import com.locationtracker.R
import com.locationtracker.databinding.FragmentRegisterBinding
import com.locationtracker.models.RegisterModel
import com.locationtracker.networks.repositories.ApiProvider
import com.locationtracker.presentations.views.activities.UserDashboard
import com.locationtracker.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class RegisterFragment : Fragment(), View.OnClickListener {


    lateinit var binding: FragmentRegisterBinding

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        binding.login.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (activity != null) {
            when (v!!.id) {
                R.id.login -> {
                    if (TextUtils.isEmpty(binding.etName.text.toString())
                            && TextUtils.isEmpty(binding.etPhone.text.toString().trim())) {
                        Toast.makeText(activity, "Please fill all the details", Toast.LENGTH_SHORT).show()
                    } else if (TextUtils.isEmpty(binding.etName.text.toString().trim())) {
                        Toast.makeText(activity, "Please enter name", Toast.LENGTH_SHORT).show()
                    } else if (TextUtils.isEmpty(binding.etPhone.text.toString().trim())) {
                        Toast.makeText(activity, "Please enter phone", Toast.LENGTH_SHORT).show()
                    } else if (binding.etPhone.text.toString().length<10) {
                        Toast.makeText(activity, "Please enter 10 digit number", Toast.LENGTH_SHORT).show()
                    } else {
                        login()
                    }
                }
            }
        }
    }

    fun login() {
        binding.includeLoader.visibility = View.VISIBLE
        val repository = ApiProvider.apiProviderRepository()
        compositeDisposable.add(
                repository.registerUser(Constants.TOKEN_VALUE,
                        binding.etName.text.toString(),
                        binding.etPhone.text.toString())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<RegisterModel>() {
                            override fun onSuccess(t: RegisterModel) {

                                Prefs.putString(Constants.USER_ID, t.userId.toString())
                                Prefs.putString(Constants.NAME, t.firstName)
                                Prefs.putString(Constants.PHONE, t.phone)

                                println("Success")
                                binding.includeLoader.visibility = View.GONE
                                val intent = Intent(activity, UserDashboard::class.java)
                                activity!!.startActivity(intent)
                                activity!!.finish()

                            }

                            override fun onError(e: Throwable) {
                                binding.includeLoader.visibility = View.GONE
                                e.printStackTrace()
                            }
                        })

        )

    }

//    override fun onPause() {
//        super.onPause()
//        compositeDisposable.dispose()
//    }
}