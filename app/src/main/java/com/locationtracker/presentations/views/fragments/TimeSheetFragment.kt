package com.locationtracker.presentations.views.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.baseutils.sharedpreference.Prefs
import com.locationtracker.R
import com.locationtracker.databinding.FragmentTimeSheetBinding
import com.locationtracker.interfaces.OnItemClickListener
import com.locationtracker.models.DeleteTimeSheetModel
import com.locationtracker.models.TimeSheetModel
import com.locationtracker.networks.repositories.ApiProvider
import com.locationtracker.presentations.adapters.TimeSheetAdapter
import com.locationtracker.presentations.views.activities.HolderActivity
import com.locationtracker.presentations.views.activities.UserDashboard
import com.locationtracker.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*

class TimeSheetFragment : Fragment(), OnItemClickListener {


    lateinit var binding: FragmentTimeSheetBinding
    lateinit var adapter: TimeSheetAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var list: LinkedList<TimeSheetModel.Data>
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    val receiverTimeSheet = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            getTimeSheet()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_time_sheet, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        activity!!.registerReceiver(receiverTimeSheet, IntentFilter(Constants.USER_SHEET))

        (activity as UserDashboard).setSupportActionBar(binding.toolbar)

        if((activity as UserDashboard).supportActionBar!=null){
            (activity as UserDashboard).supportActionBar!!.title=""
            binding.toolbar.title="Time Sheet"
        }

        list = LinkedList()

        linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.recyclerView.layoutManager = linearLayoutManager
        adapter = TimeSheetAdapter(list)
        binding.recyclerView.adapter = adapter

        adapter.setOnItemClickListener(this)

        binding.fab.setOnClickListener {
            startActivity(Intent(activity, HolderActivity::class.java)
                    .putExtra(Constants.SCREEN,Constants.SCREEN_ADD_TIME_SHEET))
        }

        getTimeSheet()

    }

    override fun onItemClick(layoutPosition: Int, check: String) {

        deleteTimeSheet(list[layoutPosition].sheetId)
    }


    private fun getTimeSheet() {
        binding.includeLoader.visibility = View.VISIBLE

        val repository = ApiProvider.apiProviderRepository()

        compositeDisposable.add(repository.getTimeSheet(Constants.TOKEN_VALUE,
                Prefs.getString(Constants.USER_ID, ""))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<TimeSheetModel>() {
                    override fun onSuccess(t: TimeSheetModel) {
                        println("Success")
                        binding.includeLoader.visibility = View.GONE
                        list=t.data
                        adapter.updateList(list)
                    }

                    override fun onError(e: Throwable) {
                        binding.includeLoader.visibility = View.GONE
                        e.printStackTrace()
                    }
                })

        )
    }

    private fun deleteTimeSheet(sheetID: String) {
        binding.includeLoader.visibility = View.VISIBLE

        val repository = ApiProvider.apiProviderRepository()

        compositeDisposable.add(repository.deleteTimeSheet(Constants.TOKEN_VALUE,
                Prefs.getString(Constants.USER_ID, ""), sheetID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<DeleteTimeSheetModel>() {
                    override fun onSuccess(t: DeleteTimeSheetModel) {
                        println("Success")
                        binding.includeLoader.visibility = View.GONE
                        getTimeSheet()
                    }

                    override fun onError(e: Throwable) {
                        binding.includeLoader.visibility = View.GONE
                        e.printStackTrace()
                    }

                }))
    }


//    override fun onPause() {
//        super.onPause()
//        compositeDisposable.dispose()
//    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(receiverTimeSheet)
    }

}
