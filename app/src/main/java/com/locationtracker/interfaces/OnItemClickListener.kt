package com.locationtracker.interfaces

interface OnItemClickListener {
    fun onItemClick(layoutPosition: Int, check: String)
}

