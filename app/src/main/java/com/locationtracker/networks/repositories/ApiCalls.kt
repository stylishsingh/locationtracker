package com.locationtracker.networks.repositories

import com.locationtracker.models.AddTimeSheetModel
import com.locationtracker.models.DeleteTimeSheetModel
import com.locationtracker.models.RegisterModel
import com.locationtracker.models.TimeSheetModel
import com.locationtracker.networks.RetrofitAPIs
import io.reactivex.Single

class ApiCalls(val apiService: RetrofitAPIs) {

    fun registerUser(token: String, phone: String,name:String): Single<RegisterModel> {
        return apiService.register(token,phone,name)
    }
    fun addTimeSheet(token: String, userID: String,sheetDate:String,sheetWork:String): Single<AddTimeSheetModel> {
        return apiService.addTimeSheet(token,userID,sheetDate,sheetWork)
    }

    fun getTimeSheet(token: String, userID: String): Single<TimeSheetModel> {
        return apiService.userSheet(token,userID)
    }

    fun deleteTimeSheet(token: String, userID: String,sheetID:String): Single<DeleteTimeSheetModel> {
        return apiService.deleteSheet(token,userID,sheetID)
    }

}