package com.locationtracker.networks.repositories

import com.locationtracker.networks.RetrofitAPIs

object ApiProvider {

    fun apiProviderRepository(): ApiCalls {
        return ApiCalls(RetrofitAPIs.Factory.create())
    }

}