package com.locationtracker.networks

import com.locationtracker.BuildConfig
import com.locationtracker.models.AddTimeSheetModel
import com.locationtracker.models.DeleteTimeSheetModel
import com.locationtracker.models.RegisterModel
import com.locationtracker.models.TimeSheetModel
import com.locationtracker.utils.Constants
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.concurrent.TimeUnit


interface RetrofitAPIs {

    @FormUrlEncoded
    @POST(Constants.REGISTER_USER)
    fun register(@Field(Constants.TOKEN) token: String,
                 @Field(Constants.NAME) name: String,
                 @Field(Constants.PHONE) phone: String): Single<RegisterModel>


    @FormUrlEncoded
    @POST(Constants.ADD_TIME_SHEET)
    fun addTimeSheet(@Field(Constants.TOKEN) token: String,
                 @Field(Constants.USER_ID) userID: String,
                 @Field(Constants.SHEET_DATE) sheetDate: String,
                 @Field(Constants.SHEET_WORK) sheetWork: String): Single<AddTimeSheetModel>

    @FormUrlEncoded
    @POST(Constants.USER_SHEET)
    fun userSheet(@Field(Constants.TOKEN) token: String,
                 @Field(Constants.USER_ID) userID: String): Single<TimeSheetModel>

    @FormUrlEncoded
    @POST(Constants.DELETE_SHEET)
    fun deleteSheet(@Field(Constants.TOKEN) token: String,
                 @Field(Constants.USER_ID) userID: String,
                 @Field(Constants.SHEET_ID) sheetID: String): Single<DeleteTimeSheetModel>

    @FormUrlEncoded
    @POST(Constants.GOOGLE_LAT_LONG)
    fun updateLatLong(@Field(Constants.TOKEN) token: String,
                 @Field(Constants.USER_ID) userID: String,
                 @Field(Constants.LAT) lat: String,
                 @Field(Constants.LONG) long: String): Single<RegisterModel>

    companion object Factory {

        private fun getClient(): OkHttpClient {
            val client: OkHttpClient
            if (BuildConfig.DEBUG) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                client = OkHttpClient.Builder().addInterceptor(interceptor).build()
            } else {
                client = OkHttpClient.Builder().build()
            }

            val builder = client.newBuilder().readTimeout(120, TimeUnit.SECONDS).connectTimeout(120, TimeUnit.SECONDS)
            return builder.build()
        }

        fun create(): RetrofitAPIs {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .client(getClient())
                    .build()


            return retrofit.create(RetrofitAPIs::class.java)
        }
    }

}