package com.locationtracker.networks

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface LocationServerAPIs {


    companion object {
        fun create(): LocationServerAPIs {

            val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://namdhari.harjeevansingh.com/app/")
                    .build()

            return retrofit.create(LocationServerAPIs::class.java)
        }
    }

}