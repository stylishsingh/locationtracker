package com.locationtracker.utils


import android.app.DatePickerDialog
import android.content.Context
import android.support.v4.app.Fragment
import com.locationtracker.presentations.views.fragments.AddTimeSheetFragment
import java.util.*

class CalendarPicker private constructor() {
    private var fragment: Fragment? = null


    fun showCalendar(context: Context, fragment: Fragment) {

        this.fragment = fragment

        val calendar = Calendar.getInstance()


        val year: Int
        val month: Int
        val day: Int

        var datePickerDialog: DatePickerDialog? = null
        try {
            year = calendar.get(Calendar.YEAR)
            month = calendar.get(Calendar.MONTH)
            day = calendar.get(Calendar.DAY_OF_MONTH)


            datePickerDialog = DatePickerDialog(context, { view, selectedYear, selectedMonth, selectedDay ->
                val selectedDate = selectedDay.toString() + "/" + (selectedMonth + 1) + "/" + selectedYear

                if (fragment is AddTimeSheetFragment) {
                    val timeSheetFragment = fragment as AddTimeSheetFragment?
                    timeSheetFragment!!.setDate(selectedDate)
                }

//                TimePicker(context, selectedDate)
            }, year, month, day)
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }

        // Calendar Minimum Range
        val calendarMinMaxDate = Calendar.getInstance()
        calendarMinMaxDate.add(Calendar.DATE, 0)

        if (datePickerDialog != null) {
            datePickerDialog.datePicker.minDate = calendarMinMaxDate.timeInMillis
            datePickerDialog.datePicker.maxDate = calendarMinMaxDate.timeInMillis
            datePickerDialog.show()
        }
    }

    companion object {

        private val calendarInstance: CalendarPicker? = null

        val instance: CalendarPicker
            get() = calendarInstance ?: CalendarPicker()
    }

}