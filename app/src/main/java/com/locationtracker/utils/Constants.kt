package com.locationtracker.utils

interface Constants {

    companion object {

        const val TOKEN_VALUE = "4f391sd0f686eba152e46e4b8841773914146e4"
        const val BASE_URL="http://namdhari.harjeevansingh.com/app/"

        /*API Calls*/
        const val REGISTER_USER = "register_user"
        const val ADD_TIME_SHEET = "add_time_sheet"
        const val USER_SHEET = "user_sheet"
        const val DELETE_SHEET = "delete_sheet"
        const val GOOGLE_LAT_LONG = "google_lat_long"

        /*API Params*/
        const val TOKEN="token"
        const val NAME="name"
        const val PHONE="phone"
        const val USER_ID="user_id"
        const val SHEET_ID="sheet_id"
        const val SHEET_DATE="sheet_date"
        const val SHEET_WORK="sheet_work"
        const val LAT="lat"
        const val LONG="long"

        /*Screens*/
        const val SCREEN="SCREEN"
        const val SCREEN_ADD_TIME_SHEET="SCREEN_ADD_TIME_SHEET"
    }

}