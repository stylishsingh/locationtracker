package com.locationtracker.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.widget.Toast

class PermissionsAndroid private constructor() {

    fun checkWriteExternalStoragePermission(activity: Activity): Boolean {
        return boolValue(ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE))
    }

    fun requestForExternalStoragePermission(activity: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(activity, "Allow Write External Storage Permission to use this functionality.", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)
        }
    }

    fun requestForExternalStoragePermission(fragment: Fragment) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(fragment.activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(fragment.activity, "Allow Write External Storage Permission to use this functionality.", Toast.LENGTH_SHORT).show()
            fragment.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)
        } else {
            fragment.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)
        }
    }

    fun checkRecordAudioPermission(activity: Activity): Boolean {
        return boolValue(ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO))
    }

    fun requestForRecordAudioPermission(activity: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(activity, "Allow Record Audio Permission to use this functionality.", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_AUDIO_PERMISSION_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_AUDIO_PERMISSION_REQUEST_CODE)
        }
    }

    //Record Audio Permission.
    fun requestForRecordAudioPermission(fragment: Fragment) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(fragment.activity!!, Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(fragment.activity, "Allow Record Audio Permission to use this functionality.", Toast.LENGTH_SHORT).show()
            fragment.requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_AUDIO_PERMISSION_REQUEST_CODE)
        } else {
            fragment.requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_AUDIO_PERMISSION_REQUEST_CODE)
        }
    }

    fun checkCameraPermission(activity: Activity): Boolean {
        return boolValue(ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA))
    }

    fun requestForCameraPermission(activity: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
            Toast.makeText(activity, "Allow Camera Permission to use this functionality.", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), CAMERA_PERMISSION_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), CAMERA_PERMISSION_REQUEST_CODE)

        }
    }

    fun requestForCameraPermission(fragment: Fragment) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(fragment.activity!!, Manifest.permission.CAMERA)) {
            Toast.makeText(fragment.activity, "Allow Camera Permission to use this functionality.", Toast.LENGTH_SHORT).show()
            fragment.requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), CAMERA_PERMISSION_REQUEST_CODE)
        } else {
            fragment.requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), CAMERA_PERMISSION_REQUEST_CODE)

        }
    }

    fun checkLocationPermission(activity: Activity): Boolean {
        return boolValue(ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION))
    }

    fun requestForLocationPermission(fragment: Fragment) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(fragment.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            Toast.makeText(fragment.activity, "Allow Location Permission to use this functionality.", Toast.LENGTH_SHORT).show()
        } else {
            fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)

        }
    }

    // Location Permission
    fun requestForLocationPermission(activity: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            Toast.makeText(activity, "Allow Location Permission to use this functionality.", Toast.LENGTH_SHORT).show()
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)

        }
    }

    // function to return true or false based on the permission result
    private fun boolValue(value: Int): Boolean {
        return if (value == PackageManager.PERMISSION_GRANTED)
            true
        else
            false
    }

    companion object {

        internal var permissionsAndroid: PermissionsAndroid? = null

        val instance: PermissionsAndroid
            get() {
                if (permissionsAndroid == null)
                    permissionsAndroid = PermissionsAndroid()
                return permissionsAndroid
            }

        // Request Code for request Permissions Must be between 0 to 255.
        //Write External Storage Permission.
        val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 100
        //Write External Storage Permission.


        //Record Audio Permission.
        val RECORD_AUDIO_PERMISSION_REQUEST_CODE = 101


        // Camera Permission
        val CAMERA_PERMISSION_REQUEST_CODE = 102
        // Camera Permission

        // Location Permission
        val LOCATION_PERMISSION_REQUEST_CODE = 103
    }

}

