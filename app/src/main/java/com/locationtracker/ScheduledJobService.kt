package com.locationtracker

import android.annotation.TargetApi
import android.os.Build
import android.util.Log
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ScheduledJobService : JobService() {

    private val TAG = ScheduledJobService::class.java.simpleName

    override fun onStopJob(params: JobParameters): Boolean {
        return false
    }

    override fun onStartJob(params: JobParameters): Boolean {
        //Offloading work to a new thread.
        Thread(Runnable { codeYouWantToRun(params) }).start()
        return true
    }

    fun codeYouWantToRun(parameters: JobParameters) {
        try {

            Log.d(TAG, "completeJob: " + "jobStarted")
            //This task takes 2 seconds to complete.
            Thread.sleep(2000)

            Log.d(TAG, "completeJob: " + "jobFinished")
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } finally {
            //Tell the framework that the job has completed and doesnot needs to be reschedule
            jobFinished(parameters, true)
        }
    }

}